package fr.ib.mickael.voyages1;

import static org.junit.Assert.*;


import org.junit.Test;

/**
 * @author mic
 *
 */
public class HorairesTest {

	@Test
	public void testGetOuverture() {
		Horaires h = new Horaires();
		int obtenu = h.getOuverture(1); //lundi
		assertEquals(10, obtenu);
		obtenu = h.getOuverture(5); //vendredi
		assertEquals(10, obtenu);
		obtenu = h.getOuverture(7); //dimanche
		assertEquals(-1, obtenu);
	}

}
