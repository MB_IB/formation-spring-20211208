/**
 * 
 */
package fr.ib.mickael.voyages1;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author mic
 *
 */
public class VoyagesMain { 

	public static void main(String[] args) {
		System.out.println("Voyages - app 1");
		Horaires h = new Horaires();
		System.out.println("Le mercredi, ouvert de "
				+h.getOuverture(3)+"h à "+h.getFermeture(3)+"h");
		System.out.println("Présentation :");
		try {
			// flux binaire en entree
			InputStream is = VoyagesMain.class.getClassLoader().
					getResourceAsStream("presentation.txt");
			// flux de ligne de texte
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String ligne = "";
			// lecture de chaque ligne ; null si fini
			while( (ligne = br.readLine()) !=null)
				System.out.println(ligne);
			br.close();
		} catch(Exception ex) {
			System.err.println("Erreur : " + ex);
			ex.printStackTrace();
		}
	}

}
