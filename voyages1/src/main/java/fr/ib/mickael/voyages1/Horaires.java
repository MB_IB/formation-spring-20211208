package fr.ib.mickael.voyages1;

/**
 * @author mic
 *
 */
public class Horaires {
	/**
	 * Ouvert du lundi au samedi de 10h à 19h
	 * 
	 * @param jourDeLaSemaine 1 (lundi) à 7 (dimanche)
	 * @return heure d'ouverture ou -1
	 */
	int getOuverture(int jourDeLaSemaine) {
		if(jourDeLaSemaine==7)
			return -1;
		else
			return 10;
	}
	
	/**
	 * Ouvert du lundi au samedi de 10h à 19h
	 * 
	 * @param jourDeLaSemaine 1 (lundi) à 7 (dimanche)
	 * @return heure de fermeture ou -1
	 */
	int getFermeture(int jourDeLaSemaine) {
		if(jourDeLaSemaine==7)
			return -1;
		else
			return 19;
	}
}
