
package fr.ib.mickael.voyages3boot;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.ib.mickael.voyages4hibernate.Destination;

/**
 * @author mic
 *
 */
@Controller
@Lazy // -  par défaut : Eager
public class DestinationsController {

	private SessionFactory sessionFactory; // modèle
	
	@RequestMapping("/destinations") // URL http://localhost:8081/destinations
	public String afficher(Model model) {		
		Session session = sessionFactory.openSession();
		List<Destination> destinations = session.
				createQuery("from Destination", Destination.class).list();
		session.close();
		model.addAttribute("destinations", destinations);
		return "destinations";		
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}		
}
