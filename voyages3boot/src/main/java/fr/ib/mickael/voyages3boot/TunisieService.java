package fr.ib.mickael.voyages3boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author mic
 *
 */
@Service // ou @Component 
public class TunisieService {
	
	private static final Logger logger = LoggerFactory.getLogger(TunisieService.class);

	public void enregistrerFormulaire(String nom, String tel) {		
		logger.info("Enregistrement de : "+nom+" / "+tel);
		//TODO enregistrer vraiment
	}
}
