
package fr.ib.mickael.voyages3boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author mic
 *
 */
@Controller
public class TunisieController {

	private TunisieService tunisieService;
	
	@RequestMapping(path="/tunisie/formulaire",method=RequestMethod.GET)
	public String afficherFormulaire() {
		// nom, tél, bouton d'envoi
		return "tunisie-formulaire";
	}
	
	@RequestMapping(path="/tunisie/formulaire",method=RequestMethod.POST)
	public String traiterFormulaire(
			@RequestParam("nom") String nom, 
			@RequestParam("tel") String tel,
			Model model) {
		if(nom.trim().length()<2 || tel.trim().length()<2) {
			model.addAttribute("message", "Erreur dans le formulaire");
			return "tunisie-formulaire";			
		} else {
			tunisieService.enregistrerFormulaire(nom, tel);
			return "redirect:/index.html";
		}
	}	

	@Autowired
	public void setTunisieService(TunisieService tunisieService) {
		this.tunisieService = tunisieService;
	}	
}
