package fr.ib.mickael.voyages3boot;

import org.springframework.stereotype.Component;

/**
 * @author mic
 *
 */
@Component // ou @Service
public class NamibieService {
	public int getPrixStandard() {
		return 1299;
	}
}
