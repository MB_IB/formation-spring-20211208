package fr.ib.mickael.voyages3boot;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.ib.mickael.voyages4hibernate.Destination;

/**
 * @author mic
 */
@Configuration
public class HibernateConfiguration {
	
	@Bean
	public SessionFactory sessionFactory() {
		Properties options = new Properties();
		options.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
		options.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
		options.put("hibernate.connection.url", "jdbc:mysql://localhost/voyages");
		options.put("hibernate.connection.username", "voyages");
		options.put("hibernate.connection.password", "password");
		options.put("hibernate.hbm2ddl.auto", "validate"); // pas create!
		options.put("hibernate.show_sql", "true");		
		SessionFactory factory = new org.hibernate.cfg.Configuration().
				addProperties(options).
				addAnnotatedClass(Destination.class).
				buildSessionFactory();				
		return factory;
	}
}
