
package fr.ib.mickael.voyages3boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author mic
 *
 */
@Controller
@Lazy // -  par défaut : Eager
public class NamibieController {

	private static final Logger logger = LoggerFactory.getLogger(NamibieController.class);
	
	private NamibieService namibieService;
	
	@RequestMapping("/namibie") // URL a entrer
	public String afficher() {		
		return "namibie";		
	}
	
	@RequestMapping("/namibie/prix")
	public String afficherPrix(Model model) {	
		model.addAttribute("service", namibieService);
		return "namibie-prix";		
	}	
	
	@RequestMapping("/namibie/404")
	public String afficher404() {
		logger.warn("Page non trouvee : /namibie/404");
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, 
				"Page pas terminée");		
	}

	@Autowired
	public void setNamibieService(NamibieService namibieService) {
		this.namibieService = namibieService;
	}
	
}
