<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Voyages : destinations</title>
	</head>
	<body>
		<h1>Voyages : destinations</h1>
		<ul>
		<c:forEach items="${destinations}" var="d">
			<li>${d}</li>
		</c:forEach>		
		</ul>		
	</body>
</html>