/**
 * 
 */
package fr.ib.mickael.voyages4hibernate;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * @author mic
 *
 */
public class VoyagesMain {

	public static void main(String[] args) {
		System.out.println("Bienvenue");
		
		// données de config
		Configuration config = new Configuration();
		// usine à session configurée
		SessionFactory sessionFactory = config.configure().buildSessionFactory();
		// session à utiliser
		Session session = sessionFactory.openSession();
		insertions(session);
		modifications(session);
		lecturesParCriteria(session);
		lecturesParHQL(session);
		lecturesParSQL(session);
		insertionsAvecRelations(session);
		lectureAvecRelations(session);
		insertionsPlusieursAPlusieurs(session);
		lecturePlusieursAPlusieurs(session);
		session.close();
	}
	
	private static void insertions(Session session) {
		Destination d1 = new Destination("Angkor Vat", "Cambodge", 3); 
		System.out.println("d1 : "+d1);
		System.out.println("* Dirty 1 ? "+session.isDirty());
		Transaction tx = session.beginTransaction();
		try {
			d1.setJours(d1.getJours()+1);
			session.save(d1);		
			System.out.println("d1 (saved) : "+d1);
			session.save(d1);	// aucun effet
			Destination d1bis = d1;
			session.save(d1bis);	// aucun effet	
			System.out.println("* Dirty 2 ? "+session.isDirty());
			d1.setJours(d1.getJours()+1); //update jours -> 5
			d1.setPays("Le "+d1.getPays()); //meme update
			// d1.setId(d1.getId()+2); // imposible a modifier
			System.out.println("* Dirty 3 ? "+session.isDirty());
			session.flush();
			System.out.println("* Dirty 4 ? "+session.isDirty());
			System.out.println("* Session "+session.getStatistics());
			
			Destination d2 = new Destination("Vientiane", "Laos", 2);
			session.save(d2);
			System.out.println("d2 (saved) : "+d2);
			System.out.println("* Session "+session.getStatistics());
			d2.setJours(5);
			session.flush(); // update ... where id=2
			session.evict(d2); // enlève d2 du cache
			// session.clear(); // vide le cache
			d2.setJours(6); // pas enregistré
						
			Destination d3 = new Destination("Damas", "Syrie", 4);
			session.save(d3);
			session.flush();
			session.delete(d3);
			
			session.save(new Destination("Taipei","Taiwan",5));
			session.save(new Destination("Lhassa","Tibet",2));
			session.save(new Destination("Osaka","Japon",3));
			session.save(new Destination("Kobe","Japon",7));
			
			tx.commit();
		} catch (Exception e) {
			System.out.println("Erreur : "+e);
			tx.rollback();
		}
	}

	private static void modifications(Session session) {
		System.out.println("** Modifications : ");
		Transaction tx = session.beginTransaction();
		session.clear();
		Destination d1 = session.load(Destination.class, 1);
		System.out.println("d1 (loaded) : "+d1);
		System.out.println(session.getStatistics().getEntityKeys());
		d1.setJours(4);
		session.flush();
		
		Destination d2 = session.get(Destination.class, 2);
		System.out.println("d2 (getted) : "+d2);
		
		Destination d293 = session.get(Destination.class, 293);
		System.out.println("d293 (getted) : "+d293); // null
		
		System.out.println("** Autres modifications : ");
		session.clear();
		
		d1.setJours(10);
		// session.update(d1); // java=>cache, bd
		// session.saveOrUpdate(d1); // java=>cache, bd (suivant id==0)
		session.refresh(d1); // bd=>cache, java
		System.out.println("d1 (refreshed) : "+d1); // jours = ????
		
		tx.commit();
	}

	private static void lecturesParCriteria(Session session) {
		System.out.println("Lectures par criteria query :");
		session.clear();
		// objectif SQL : select * from destinations where id>1 order by nom
		// constructeur de requete
		CriteriaBuilder cb = session.getCriteriaBuilder();
		// requête
		CriteriaQuery<Destination> toutesSaufPremiere =	cb.createQuery(Destination.class);
		// point de départ (table) de la requete
		Root<Destination> table = toutesSaufPremiere.from(Destination.class);
		// select * (tout dans la table)
		toutesSaufPremiere.select(table);
		toutesSaufPremiere.orderBy(cb.asc(table.get("nom"))); // asc = ascendant
		toutesSaufPremiere.where(cb.gt(table.<Integer>get("id"), 1)); // gt = greater than
		List<Destination> listToutesSaufPremiere = session.
				createQuery(toutesSaufPremiere).list();
		// tout afficher
		System.out.println("* Par criteria :");
		for(Destination d:listToutesSaufPremiere) {
			System.out.println(" - "+d);
		}					
	}

	private static void lecturesParHQL(Session session) {
		System.out.println("Lectures par HQL/JPQL :");
		session.clear();	
		String toutesSaufPremiereHQL = 
				"from Destination where id>1 order by nom"; 
		List<Destination> listToutesSaufPremiere = session.
				createQuery(toutesSaufPremiereHQL, Destination.class).list();
		// tout afficher
		System.out.println("* Par HQL :");
		for(Destination d:listToutesSaufPremiere) {
			System.out.println(" - "+d);
		}		
		
		int maxJours = 3;
		String rapidesHQL = "from Destination where jours <= :j ";
		List<Destination> listRapides = session.
				createQuery(rapidesHQL, Destination.class).
				setParameter("j", maxJours).
				list();
		System.out.println("* Par HQL - destinations rapides :");
		for(Destination d:listRapides) {
			System.out.println(" - "+d);
		}	
		
		int minJours = 2;
		maxJours = 4;
		String debutPays = "Ja";
		String moyennesHQL = "from Destination "+
				//"where jours >= :minj and jours <= :maxj ";
				// NON : "where (jours between "+minJours+" and "+maxJours+") and (pays not like :dp)";
				"where (jours between :minj and :maxj) and (pays not like :dp)";				
		List<Destination> listMoyennes = session.
				createQuery(moyennesHQL, Destination.class).
				setParameter("minj", minJours).
				setParameter("maxj", maxJours).
				setParameter("dp", debutPays+"%").
				list();						
		System.out.println("* Par HQL - destinations moyennes dont le pays ne "+
				"commence pas par \"Ja\" :");
		for(Destination d:listMoyennes) {
			System.out.println(" - "+d);
		}	
		
		String compteHQL = "select count(*) from Destination";
		long compte = (Long) session.createQuery(compteHQL).uniqueResult();
		System.out.println("Il y a "+compte+" destinations.");		
	}

	@SuppressWarnings("unchecked")
	private static void lecturesParSQL(Session session) {
		System.out.println("Lectures par SQL :");
		session.clear();	
		String toutesSaufPremiereSQL = 
				"select * from destinations where id>1 order by nom"; 
		List<Destination> listToutesSaufPremiere = session.
				createSQLQuery(toutesSaufPremiereSQL).
				addEntity(Destination.class).list();
		// tout afficher
		System.out.println("* Par SQL :");
		for(Destination d:listToutesSaufPremiere) {
			System.out.println(" - "+d);
		}		
		
	}

	private static void insertionsAvecRelations(Session session) {
		System.out.println("** Insertions avec relations : ");
		Transaction tx = session.beginTransaction();
		
		Attraction a1 = new Attraction("Tour centrale", "Sanctuaire", 0);
		Destination d1 = session.load(Destination.class, 1);
		a1.setDestination(d1);
		session.save(a1);
		
		Attraction a2 = new Attraction("Avenue des nagas", "Galeries", 0);
		a2.setDestination(d1);
		session.save(a2);
		
		Attraction a3 = new Attraction("Marché de nuit", "Shilin", 0);
		a3.setDestination(session.load(Destination.class, 4));
		session.save(a3);
		
		Attraction a4 = new Attraction("Palais de l'hamonie suprême", "Centre", 12.5);
		Destination d10 = new Destination("Hué", "Vietnam", 4);
		a4.setDestination(d10);
		session.save(a4);
		session.save(d10);
		
		tx.commit();		
	}

	private static void lectureAvecRelations(Session session) {	
		System.out.println("** Lectures avec relations : ");
		session.clear();
		Attraction a1 = session.load(Attraction.class, 1); 
		System.out.println("Pays de la 1ere attraction : "+
				a1.getDestination().getPays());
		System.out.println(session.getStatistics()); // "entity count" = 2
		// OK : Destination d1 = a1.getDestination();
		
		String toutesHQL = "from Attraction order by destination.pays";
		List<Attraction> toutesAttractions = session.createQuery(toutesHQL, 
				Attraction.class).list();
		System.out.println("Toutes les attractions, ordrées par pays");
		for(Attraction a:toutesAttractions) {
			System.out.println(" - "+a);
		}
		
		String destinationsHQL = "select destination.nom, max(prix) "+
				"from Attraction group by destination.id";
		List<Object[]> destinations = session.createQuery(destinationsHQL, 
				Object[].class).list();
		System.out.println("Noms des destinations et prix de leur attraction la + chère :");
		for(Object[] d:destinations) {
			System.out.println(" - "+d[0]+" "+d[1]);
		}
		
		String nomPays = "Vietnam";
		String totalParPays = "select sum(prix) from Attraction "+
				"where destination.pays like :np";
		Double prixTotal = (Double)session.createQuery(totalParPays, Double.class).
			setParameter("np", nomPays).
			uniqueResult();
		System.out.println("Total des prix pour "+nomPays+" : "+prixTotal+"e");
		
	}

	private static void insertionsPlusieursAPlusieurs(Session session) {
		System.out.println("** Insertions avec relations *-* : ");
		Transaction tx = session.beginTransaction();
		
		Voyage v1  = new Voyage("Merveilles du Cambodge", LocalDate.of(2022,4,14));
		Destination d1 = session.load(Destination.class, 1);
		v1.getEtapes().add(d1);
		session.save(v1);
		
		// ajouter des voyages : au moins 1  voyage de plusieurs destinations et
		// au moins une destination visitée par plusieurs voyages
		List<Destination> destinations = session.
				createQuery("from Destination order by id", Destination.class).list();
		Voyage v2  = new Voyage("Tour de l'Indochine", LocalDate.of(2022,2,1));
		v2.getEtapes().add(destinations.get(0));
		v2.getEtapes().add(destinations.get(1));
		session.save(v2);
		Voyage v3  = new Voyage("Le Japon ancestral", LocalDate.of(2022,6,3));
		v3.getEtapes().add(destinations.get(4));
		v3.getEtapes().add(destinations.get(5));
		session.save(v3);
		
		tx.commit();		
	}
	
	private static void lecturePlusieursAPlusieurs(Session session) {	
		System.out.println("** Lectures avec relations : ");
		session.clear();
		
		List<Voyage> voyages = session.createQuery("from Voyage order by nom", 
				Voyage.class).list();
		System.out.println("Tous les voyages et leurs destinations ");
		for(Voyage v:voyages) {
			System.out.println(" - "+v+" : ");
			for(Destination d:v.getEtapes()) {
				System.out.println(" --- "+d);
			}
		}
		
		String pays = "%Cambodge%";
		//String hqlEchoue = "from Voyage where etapes.pays = :p";
		String voyageAvecPaysHql = "select distinct v from Voyage as v join v.etapes as e "+
				"where e.pays like :p";		
		List<Voyage> voyageAvecPays = session.createQuery(voyageAvecPaysHql, 
				Voyage.class).setParameter("p", pays).list();
		System.out.println("Tous les voyages passant par un pays : ");
		for(Voyage v:voyageAvecPays) {
			System.out.println(" - "+v);
		}
	}
}
