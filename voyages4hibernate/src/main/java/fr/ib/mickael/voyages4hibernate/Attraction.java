
package fr.ib.mickael.voyages4hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author mic
 *
 */
@Entity
@Table(name="attractions")
public class Attraction {
	private int id;
	private String nom;
	private String lieu;
	private double prix;
	private Destination destination;
	
    public Attraction() {
        this(null, null, 0);
    }
    
    public Attraction(String n, String l, double p) {
        id = 0;
        nom = n;
        lieu = l;
        prix = p;
    }

    @Override
    public String toString() {
        return id+" "+nom+" ("+lieu+") : "+prix+"e";
    } 
	
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(length=40, nullable=false)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Column(length=40, nullable=true)
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	@Column(nullable=false, precision=5, scale=2)
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	public Destination getDestination() {
		return destination;
	}
	public void setDestination(Destination destination) {
		this.destination = destination;
	}
	
	
}
