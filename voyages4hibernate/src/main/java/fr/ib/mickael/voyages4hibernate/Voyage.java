package fr.ib.mickael.voyages4hibernate;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * @author mic
 *
 */
@Entity
@Table(name="voyages")
public class Voyage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nom;
	private LocalDate depart;
	private List<Destination> etapes;
	
	public Voyage() {
		this(null, null);
	}
	public Voyage(String n, LocalDate d) {
		id = 0;
		nom = n;
		depart = d;
		etapes = new ArrayList<Destination>();
	}
	@Override
	public String toString() {
		return id+" : "+nom+" ("+depart+")";
	}
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(length=50,nullable=false)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Column(nullable=true)
	public LocalDate getDepart() {
		return depart;
	}
	public void setDepart(LocalDate depart) {
		this.depart = depart;
	}
	@ManyToMany
	public List<Destination> getEtapes() {
		return etapes;
	}
	public void setEtapes(List<Destination> etapes) {
		this.etapes = etapes;
	}
	
	
}
