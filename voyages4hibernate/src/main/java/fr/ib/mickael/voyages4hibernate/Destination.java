package fr.ib.mickael.voyages4hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author mic
 */
@Entity
@Table(name="destinations")
public class Destination implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String nom;
	private String pays;
	private int jours;
	
	public Destination() {
		//nom = pays = null;
		//jours = 0;
		this(null, null, 0);
	}
	
	public Destination(String n, String p, int j) {
		id = 0;
		nom = n;
		pays = p;		
//		if(j<0)
//			throw new IllegalArgumentException();
//		else
//			jours = j;
		setJours(j);
	}
	
	@Override
	public String toString() {
		return id+" : "+nom+" ("+pays+") : "+jours+"j";
	}

	@Column(name="id") // valeur par défaut
	@Id // clé primaire : unique, index voire généré automatiquement
	@GeneratedValue(strategy=GenerationType.IDENTITY) // auto_increment
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(length=40,nullable=false,unique=false)
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(length=25,nullable=false)
	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	@Column(nullable=false)
	public int getJours() {
		return jours;
	}

	public void setJours(int jours) {
		if(jours<0)
			throw new IllegalArgumentException();
		else
			this.jours = jours;		
	}
	
	@Transient //ne pas faire de colonne pour ça
	public int getSemaines() {
		return jours/7;
	}

	public void setSemaines(int semaines) {
//		if(semaines<0)
//			throw new IllegalArgumentException();
//		else
//			jours = semaines * 7;
		setJours(semaines * 7);
	}
}
