package fr.ib.mickael.voyages2spring;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

/**
 * @author mic
 *
 */
public class VoyagesMain {

	public static void main(String[] args) {
		System.out.println("Voyages 2 : bienvenue");
		AbstractApplicationContext ctx =
				new FileSystemXmlApplicationContext("classpath:springcontext.xml");
		ctx.registerShutdownHook(); // ferme le contexte quand l'appli s'arrete
		//faireAmerique(ctx);
		faireEurope(ctx);
	}
	private static void faireAmerique(AbstractApplicationContext ctx) {
		Resource rAlaska = ctx.getResource("classpath:alaska.txt");
		try {
			InputStream isAlaska = rAlaska.getInputStream();
			// Afficher le contenu du fichier
			BufferedReader br = new BufferedReader(new InputStreamReader(isAlaska));
			String ligne = "";
			// lecture de chaque ligne ; null si fini
			while( (ligne = br.readLine()) !=null) {
				System.out.println(ligne);
			}
			br.close();
		} catch(Exception ex) {
			System.err.println("Erreur : "+ex);
		}
		
		CanadaService canadaService = (CanadaService) ctx.getBean("canadaService");
		System.out.println("12j au Canada : "+canadaService.getPrix(12)+"€");
		UsaService usaService = (UsaService) ctx.getBean("usaService");
		System.out.println("12j aux USA : "+usaService.getPrix(12)+"€");
		MexiqueService mexiqueService = (MexiqueService) ctx.getBean("mexiqueService");
		System.out.println("14j au Mexique : "+mexiqueService.getPrix()+"€");
		Integer placesDisponibles = (Integer)ctx.getBean("dispos");
		System.out.println("Places disponibles : "+placesDisponibles);
		LocalDate aujourdhui = (LocalDate)ctx.getBean("today");
		System.out.println("Aujourd'hui : "+aujourdhui);
		LocalDate depart = (LocalDate)ctx.getBean("depart");
		System.out.println("Prochain départ : "+depart);
		String mexiqueInfo = (String)ctx.getBean("mexiqueInfo");
		System.out.println(mexiqueInfo);		
		BresilService bresilService = (BresilService) ctx.getBean("bresilService");
		System.out.println(bresilService.toString());
		
		MexiqueService mexiqueServiceYucatan = (MexiqueService) ctx.getBean("mexiqueService");
		mexiqueServiceYucatan.setPrix(1450);
		System.out.println("14j au Yucatan : "+mexiqueServiceYucatan.getPrix()+"€");
		System.out.println("Date : "+mexiqueServiceYucatan.getDepart());
		System.out.println("(rappel - au Mexique : "+mexiqueService.getPrix()+"€)");
		System.out.println("(et départ : "+mexiqueService.getDepart()+")");
		
	}

	private static void faireEurope(AbstractApplicationContext ctx) {
		// EspagneService espagneService = (EspagneService) ctx.getBean("espagneService");
		// espagneService.remplirDestinations();
		// espagneService.afficheDestinations();
		ItalieService italieService = (ItalieService) ctx.getBean("italieService");
		italieService.afficherDestinations();
	}
}
