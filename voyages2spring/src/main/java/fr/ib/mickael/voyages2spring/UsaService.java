package fr.ib.mickael.voyages2spring;

/**
 * @author mic
 *
 */
public class UsaService {
	private int journee;
	private int[] avion; // 0 = aller, 1 = retour
	private int promo; // 0 à 100
	
	public int getPrix(int jours) {
		return (100-promo)*(journee*jours+avion[0]+avion[1])/100;
	}
	
	public int getJournee() {
		return journee;
	}
	public void setJournee(int journee) {
		this.journee = journee;
	}
	public int[] getAvion() {
		return avion;
	}
	public void setAvion(int[] avion) {
		this.avion = avion;
	}
	public int getPromo() {
		return promo;
	}
	public void setPromo(int promo) {
		this.promo = promo;
	}
	
	
}
