package fr.ib.mickael.voyages2spring;

/**
 * @author mic
 *
 */
public class BresilService {
	@Override
	public String toString() {
		return "BresilService";
	}
	public void init() {
		System.out.println("* Init Brésil");
	}
	public void destroy() {
		System.out.println("* Destroy Brésil");
	}
}
