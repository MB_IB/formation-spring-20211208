package fr.ib.mickael.voyages2spring;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * @author mic
 *
 */
public class EspagneService {
	private JdbcTemplate sqlTemplate;
	public EspagneService(JdbcTemplate st) {
		sqlTemplate = st;
	}
	public void remplirDestinations() {
		System.out.println("Remplissage");
		String sqlCreate = "CREATE TABLE IF NOT EXISTS destination(nom TEXT, region TEXT)";
		sqlTemplate.execute(sqlCreate);
		/*
		// ne pas faire :
		//String nom = "Barcelone";
		String nom = "Barcelone\"); DROP TABLE destination; --";
		String region = "Catalogne";
		String sqlInsert = "INSERT INTO destination(nom, region) VALUES (\""+nom+"\", \""+region+"\")";
		sqlTemplate.update(sqlInsert);
		*/
		String sqlInsert = "INSERT INTO destination(nom, region) VALUES (?,?)";
		sqlTemplate.update(sqlInsert, "Barcelone", "Catalogne");
		sqlTemplate.update(sqlInsert, "Cordoue", "Andalousie");
		sqlTemplate.update(sqlInsert, "Madrid", "Castille");
	}
	public void afficheDestinations() {
		System.out.println("Destinations :");
		// toutes, triées par region puis par nom
		String sqlSelect = "SELECT nom, region FROM destination ORDER BY region, nom";
		SqlRowSet set = sqlTemplate.queryForRowSet(sqlSelect);
		while(set.next()) {
			System.out.println(set.getString(1)+" "+set.getString(2));
		}
		
	}
}
