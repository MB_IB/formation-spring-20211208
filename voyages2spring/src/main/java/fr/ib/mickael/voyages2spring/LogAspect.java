
package fr.ib.mickael.voyages2spring;

import java.time.LocalDateTime;

/**
 * @author mic
 *
 */
public class LogAspect {
	public void ecrire() {
		System.out.println(LocalDateTime.now()+" : LOG");
	}
}
