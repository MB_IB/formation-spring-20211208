package fr.ib.mickael.voyages2spring;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author mic
 *
 */
@Repository
public interface DestinationRepository extends CrudRepository<Destination, String> {}
