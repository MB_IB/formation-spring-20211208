package fr.ib.mickael.voyages2spring;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author mic
 *
 */
public class ChronoAspect {
	public Object mesurer(ProceedingJoinPoint pjp) throws Throwable {
		long debut = System.nanoTime();
		Object resultat = pjp.proceed();
		long fin = System.nanoTime();
		System.out.println("***** fonction "+pjp.getSignature().getName()+" : "+
				(fin-debut)/1000+"µs *****");
		//return ((Integer)resultat)*2;
		return resultat;
	}
}
