package fr.ib.mickael.voyages2spring;

import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

/**
 * @author mic
 *
 */
@EnableRedisRepositories("fr.ib.mickael.voyages2spring")
public class ItalieService {
	
	private DestinationRepository destinationRepository;
	
	public void afficherDestinations() {
		System.out.println("Destinations :");
		Destination d1 = new Destination();
		d1.setNom("Rome"); d1.setRegion("Latium");
		Destination d2 = new Destination();
		d2.setNom("Aoste"); d2.setRegion("Val d'Aoste");
		destinationRepository.deleteAll();
		destinationRepository.save(d1);
		destinationRepository.save(d2);
		
		for(Destination d : destinationRepository.findAll()) {
			System.out.println(" - "+d);
		}
	}

	public DestinationRepository getDestinationRepository() {
		return destinationRepository;
	}

	public void setDestinationRepository(DestinationRepository destinationRepository) {
		this.destinationRepository = destinationRepository;
	}
}
