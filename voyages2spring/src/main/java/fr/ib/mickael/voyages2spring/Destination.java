package fr.ib.mickael.voyages2spring;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

/**
 * @author mic
 *
 */
@RedisHash("Destination")
public class Destination {
	private String nom;
	private String region;
	@Override
	public String toString() {
		return nom+" ("+region+")";
	}
	@Id
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
}
