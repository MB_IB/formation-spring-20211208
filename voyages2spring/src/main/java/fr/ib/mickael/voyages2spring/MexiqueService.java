/**
 * 
 */
package fr.ib.mickael.voyages2spring;

import java.time.LocalDate;

/**
 * @author mic
 *
 */
public class MexiqueService {
	private double prix;
	private LocalDate depart;

	public MexiqueService() {
		prix = 0;
	}
	
	public MexiqueService(double p) {
		prix = p;
	}
	
	public MexiqueService(double p, LocalDate d) {
		prix = p;
		depart = d;
	}
	
	@Override
	public String toString() {
		return "Mexique : "+prix+"€, départ le "+depart;
	}
	
	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public LocalDate getDepart() {
		return depart;
	}

	public void setDepart(LocalDate depart) {
		this.depart = depart;
	}	
	
}
